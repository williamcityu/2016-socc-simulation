/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 Polytechnic Institute of NYU
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Adrian S. Tam <adrian.sw.tam@gmail.com>
 * Author: Fan Wang <amywangfan1985@yahoo.com.cn>
 * Modified by Hong Xu <henry.xu@cityu.edu.hk>

 */

//#define __STDC_LIMIT_MACROS 1
#include <sstream>
#include <stdint.h>
#include <stdlib.h>

#include "fat-tree-helper.h"

#include "ns3/log.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/queue.h"
#include "ns3/sp-net-device.h"
#include "ns3/boolean.h"
#include "ns3/uinteger.h"
#include "ns3/point-to-point-channel.h"
#include "ns3/ipv4-address-generator.h"
#include "ns3/ipv4-hash-routing-helper.h"
#include "ns3/config.h"
#include "ns3/abort.h"
#include "ns3/double.h"
#include "ns3/point-to-point-helper.h"

namespace ns3 {
NS_LOG_COMPONENT_DEFINE ("FatTreeHelper");

NS_OBJECT_ENSURE_REGISTERED (FatTreeHelper);

unsigned FatTreeHelper::m_size = 0;	// Defining static variable

TypeId
FatTreeHelper::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::FatTreeHelper")
    .SetParent<Object> ()
    .AddAttribute ("HeDataRate",
                   "The default data rate for point to point links",
                   DataRateValue (DataRate ("1Gbps")),
                   MakeDataRateAccessor (&FatTreeHelper::m_heRate),
                   MakeDataRateChecker ())
    .AddAttribute ("EaDataRate",
                   "The default data rate for point to point links",
                   DataRateValue (DataRate ("1Gbps")),
                   MakeDataRateAccessor (&FatTreeHelper::m_eaRate),
                   MakeDataRateChecker ())
    .AddAttribute ("AcDataRate",
                   "The default data rate for point to point links",
                   DataRateValue (DataRate ("1Gbps")),
                   MakeDataRateAccessor (&FatTreeHelper::m_acRate),
                   MakeDataRateChecker ())
    .AddAttribute ("HeDelay", "Transmission delay through the channel",
                   TimeValue (NanoSeconds (500)),
                   MakeTimeAccessor (&FatTreeHelper::m_heDelay),
                   MakeTimeChecker ())
    .AddAttribute ("EaDelay", "Transmission delay through the channel",
                   TimeValue (NanoSeconds (500)),
                   MakeTimeAccessor (&FatTreeHelper::m_eaDelay),
                   MakeTimeChecker ())
    .AddAttribute ("AcDelay", "Transmission delay through the channel",
                   TimeValue (NanoSeconds (500)),
                   MakeTimeAccessor (&FatTreeHelper::m_acDelay),
                   MakeTimeChecker ())
    .AddAttribute ("ToROsp", "oversubscription ratio at ToR tier for the network",
                    UintegerValue (1),
                    MakeUintegerAccessor (&FatTreeHelper::m_torosp),                    
                    MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("CoreOsp", "oversubscription ratio at core tier for the network",
                    UintegerValue (1),
                    MakeUintegerAccessor (&FatTreeHelper::m_coreosp),                    
                    MakeUintegerChecker<uint32_t> ())
 ;

  return tid;
}

FatTreeHelper::FatTreeHelper(unsigned N)
{
  m_size = N;                                                                     
  m_channelFactory.SetTypeId ("ns3::PointToPointChannel");
  m_ppFactory.SetTypeId ("ns3::PointToPointNetDevice"); 
  // use droptail queue by default
  m_queueFactory.SetTypeId ("ns3::DropTailQueue");
}

FatTreeHelper::~FatTreeHelper()
{
}

void 
FatTreeHelper::SetQueue (std::string type,
                              std::string n1, const AttributeValue &v1,
                              std::string n2, const AttributeValue &v2,
                              std::string n3, const AttributeValue &v3,
                              std::string n4, const AttributeValue &v4)
{
  m_queueFactory.SetTypeId (type);
  m_queueFactory.Set (n1, v1);
  m_queueFactory.Set (n2, v2);
  m_queueFactory.Set (n3, v3);
  m_queueFactory.Set (n4, v4);
}

/* Create the whole topology */
void
FatTreeHelper::Create()
{

	const unsigned N = m_size;
	const unsigned numST = 2*N;
	const unsigned numCore = N*(N/m_coreosp);
	const unsigned numAggr = numST * N;
	const unsigned numEdge = numST * N;
	const unsigned numHost = numEdge * N * m_torosp;
	const unsigned numTotal= numCore + numAggr + numEdge + numHost;

	/*
	 * Create nodes and distribute them into different node container.
	 * We create 5N^2+2N^3 nodes at a batch, where the first 4N^2 nodes are the
	 * edge and aggregation switches. In each of the 2N subtrees, first N nodes are
	 * edges and the remaining N are aggregations. The last N^2 nodes in the
	 * first 5N^2 nodes are core switches. The last 2N^3 nodes are end hosts.
	 */
	NS_LOG_LOGIC ("Creating fat-tree nodes.");
	m_node.Create(numTotal);

	for(unsigned j=0;j<2*N;j++) { // For every subtree
		for(unsigned i=j*2*N; i<=j*2*N+N-1; i++) { // First N nodes
			m_edge.Add(m_node.Get(i));
		}
		for(unsigned i=j*2*N+N; i<=j*2*N+2*N-1; i++) { // Last N nodes
			m_aggr.Add(m_node.Get(i));
		}
	};
	for(unsigned i=4*N*N; i<4*N*N + N*(N/m_coreosp); i++) {
		m_core.Add(m_node.Get(i));
	};
	for(unsigned i=4*N*N + N*(N/m_coreosp); i<numTotal; i++) {
		m_host.Add(m_node.Get(i));
	};

	/*
	 * Connect nodes by adding netdevice and set up IP addresses to them.
	 *
	 * The formula is as follows. We have a fat tree of parameter N, and
	 * there are six categories of netdevice, namely, (1) on host;
	 * (2) edge towards host; (3) edge towards aggr; (4) aggr towards
	 * edge; (5) aggr towards core; (6) on core. There are 2N^3 devices
	 * in each category which makes up to 12N^3 netdevices. The IP addrs
	 * are assigned in the subnet 10.0.0.0/8 with the 24 bits filled as
	 * follows: (Assume N is representable in 6 bits)
	 *
	 * Address         Scheme
	 *               | 7 bit      | 1 bit |  6 bit  | 2 bit | 8 bit   |
	 * Host (to edge)| Subtree ID |   0   | Edge ID |  00   | Host ID |
	 * Edge (to host)| Subtree ID |   0   | Edge ID |  10   | Host ID |
	 * Edge (to aggr)| Subtree ID |   0   | Edge ID |  11   | Aggr ID |
	 * Agg. (to edge)| Subtree ID |   0   | Edge ID |  01   | Aggr ID |
	 *
	 * Address         Scheme
	 *               | 7 bit  | 1 bit | 2 bit |  6 bit  | 8 bit   |
	 * Agg. (to core)| Subtree ID |   1   |  00   | Aggr ID | Core ID |
	 * Core (to aggr)| Subtree ID |   1   |  01   | Core ID | Aggr ID |
	 *
	 * All ID are numbered from 0 onward. Subtree ID is numbered from left to
	 * right according to the fat-tree diagram. Host ID is numbered from
	 * left to right within the same attached edge switch. Edge and aggr
	 * ID are numbered within the same subtree. Core ID is numbered with a
	 * mod-N from left to right according to the fat-tree diagram.
	 */
	NS_LOG_LOGIC ("Creating connections and set-up addresses.");
	Ipv4HashRoutingHelper hashHelper;
	InternetStackHelper internet;
	internet.SetRoutingHelper(hashHelper);
	internet.Install (m_node);
	
	m_ppFactory.Set ("DataRate", DataRateValue(m_heRate));          /* Host to Edge */
	m_channelFactory.Set ("Delay", TimeValue(m_heDelay));
	for (unsigned j=0; j<numST; j++) { // For each subtree
		for(unsigned i=0; i<N; i++) { // For each edge
			for(unsigned m=0; m<N*m_torosp; m++) { // For each port of edge
				// Connect edge to host
				Ptr<Node> eNode = m_edge.Get(j*N+i);
				Ptr<Node> hNode = m_host.Get(j*N*N*m_torosp+i*N*m_torosp+m);
				NetDeviceContainer devices;
				devices = InstallCpCp(eNode, hNode);
				// Set routing for end host: Default route only
				Ptr<HashRouting> hr = hashHelper.GetHashRouting(hNode->GetObject<Ipv4>());
				hr->AddRoute(Ipv4Address(0U), Ipv4Mask(0U), 1);                               
				// Set IP address for end host
				uint32_t address = (((((((10<<7)+j)<<7)+i)<<2)+0x0)<<8)+m;
				AssignIP(devices.Get(1), address, m_hostIface);

                                // Debug code: output tace file
//                                if (Ipv4Address(address) == Ipv4Address("10.2.0.0")) 
//                                {               
//                                        // Enable a promiscuous pcap trace to see what is coming and going on host device
//                                        PointToPointHelper hostLink;
//                                        hostLink.EnablePcap("hostTrace", devices.Get(1), true, true);
//                                }

				// Set routing for edge switch
				hr = hashHelper.GetHashRouting(eNode->GetObject<Ipv4>());
				hr->AddRoute(Ipv4Address(address), Ipv4Mask(0xFFFFFFFFU), m+1);

				// Set IP address for edge switch
				address = (((((((10<<7)+j)<<7)+i)<<2)+0x2)<<8)+m;
				AssignIP(devices.Get(0), address, m_edgeIface);
			};
		};
	};
	m_ppFactory.Set ("DataRate", DataRateValue(m_eaRate));	/* Edge to Aggr */
	m_channelFactory.Set ("Delay", TimeValue(m_eaDelay));
	for (unsigned j=0; j<numST; j++) { // For each subtree
		for(unsigned i=0; i<N; i++) { // For each edge
			for(unsigned m=0; m<N; m++) { // For each aggregation
				// Connect edge to aggregation
				Ptr<Node> aNode = m_aggr.Get(j*N+m);
				Ptr<Node> eNode = m_edge.Get(j*N+i);
				NetDeviceContainer devices;
				devices = InstallCpCp(eNode, aNode);                               
				// Set IP address for aggregation switch
				uint32_t address = (((((((10<<7)+j)<<7)+i)<<2)+0x1)<<8)+m;

				AssignIP(devices.Get(1), address, m_aggrIface);

				// Set routing for aggregation switch
				Ptr<HashRouting> hr = hashHelper.GetHashRouting(aNode->GetObject<Ipv4>());
				hr->AddRoute(Ipv4Address(address & 0xFFFFFC00U), Ipv4Mask(0xFFFFFC00U), i+1);

				// Set IP address for edge switch
				address = (((((((10<<7)+j)<<7)+i)<<2)+0x3)<<8)+m;

				AssignIP(devices.Get(0), address, m_edgeIface);
			} ;
		};
	};
	m_ppFactory.Set ("DataRate", DataRateValue(m_acRate));	/* Aggr to Core */
	m_channelFactory.Set ("Delay", TimeValue(m_acDelay));
	for(unsigned j=0; j<numST; j++) { // For each subtree
		for(unsigned i=0; i<N; i++) { // For each aggr
			for(unsigned m=0; m<N/m_coreosp; m++) { // For each port of aggr
				// Connect aggregation to core
				Ptr<Node> cNode = m_core.Get(i*(N/m_coreosp)+m);
				Ptr<Node> aNode = m_aggr.Get(j*N+i);
				NetDeviceContainer devices = InstallCpCp(cNode, aNode);
				// Set IP address for aggregation switch
				uint32_t address = (((((((10<<7)+j)<<3)+0x4)<<6)+i)<<8)+m;
				AssignIP(devices.Get(1), address, m_aggrIface);
				// Set routing for core switch
				Ptr<HashRouting> hr = hashHelper.GetHashRouting(cNode->GetObject<Ipv4>());
				hr->AddRoute(Ipv4Address(address & 0xFFFE0000U), Ipv4Mask(0xFFFE0000U), j+1);
				// Set IP address for core switch
				address = (((((((10<<7)+j)<<3)+0x5)<<6)+m)<<8)+i;
				AssignIP(devices.Get(0), address, m_coreIface);
			};
		};
	};

#ifdef NS3_LOG_ENABLE
	if (! g_log.IsEnabled(ns3::LOG_DEBUG)) {
		return;
	};
	for(unsigned i = 0; i< numTotal; i++) {
		Ptr<Ipv4> m_ipv4 = m_node.Get(i)->GetObject<Ipv4>();
		for(unsigned j=1; j<m_ipv4->GetNInterfaces(); j++) {
			for(unsigned k=0; k<m_ipv4->GetNAddresses(j); k++) {
				NS_LOG_DEBUG("Addr of node " << i << " iface " << j << ": " << m_ipv4->GetAddress(j,k));
			}
		}
	}
#endif
} // FatTreeHelper::Create()

void
FatTreeHelper::AssignIP (Ptr<NetDevice> c, uint32_t address, Ipv4InterfaceContainer &con)
{
	NS_LOG_FUNCTION_NOARGS ();

	Ptr<Node> node = c->GetNode ();
	NS_ASSERT_MSG (node, "FatTreeHelper::AssignIP(): Bad node");

	Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
	NS_ASSERT_MSG (ipv4, "FatTreeHelper::AssignIP(): Bad ipv4");

	int32_t ifIndex = ipv4->GetInterfaceForDevice (c);
	if (ifIndex == -1) {
		ifIndex = ipv4->AddInterface (c);
	};
	NS_ASSERT_MSG (ifIndex >= 0, "FatTreeHelper::AssignIP(): Interface index not found");

	Ipv4Address addr(address);
	Ipv4InterfaceAddress ifaddr(addr, 0xFFFFFFFF);
	ipv4->AddAddress (ifIndex, ifaddr);
	ipv4->SetMetric (ifIndex, 1);
	ipv4->SetUp (ifIndex);
	con.Add (ipv4, ifIndex);
	Ipv4AddressGenerator::AddAllocated (addr);
}

NetDeviceContainer
FatTreeHelper::InstallCpCp (Ptr<Node> a, Ptr<Node> b)
{
	NetDeviceContainer container;

	Ptr<PointToPointNetDevice> devA = m_ppFactory.Create<PointToPointNetDevice> ();
	devA->SetAddress (Mac48Address::Allocate ());
	a->AddDevice (devA);
	// we need to add a queue to each netdevice. default is droptail queue
	Ptr<Queue> queueA = m_queueFactory.Create<Queue> ();
  	devA->SetQueue (queueA);
	Ptr<PointToPointNetDevice> devB = m_ppFactory.Create<PointToPointNetDevice> ();
	devB->SetAddress (Mac48Address::Allocate ());
	b->AddDevice (devB);
	Ptr<Queue> queueB = m_queueFactory.Create<Queue> ();
  	devB->SetQueue (queueB);
	Ptr<PointToPointChannel> channel = m_channelFactory.Create<PointToPointChannel> ();
	devA->Attach (channel);
	devB->Attach (channel);
	container.Add (devA);
	container.Add (devB);

	return container;
}

}//namespace
