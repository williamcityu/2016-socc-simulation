# Copyright (c) 2014 Hong Xu
# 
# This script reads all the result xml files and output FCT statistics
# for Tinyflow performance evaluation. The FCT is taken by putting all flows
# together, and calculate the aggregated mean and high-percentile values. This
# calculation is preferred. 
# 
# The output format is as follows:
# Each load is two rows, first w.o. tinyflow, then w. tinyflow
# mice mean, mice tail, elephant mean, elephant tail, totall mean, total tail
# 
# For load from 0.1 to 0.8. Each load with 10 runs.
#  
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Author: Hong Xu, <henry.xu@cityu.edu.hk>

from __future__ import division
from scipy import stats
from array import array
import numpy
import sys
import os
import math

try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    from xml.etree import ElementTree

def parse_time_ns(tm):
    if tm.endswith('ns'):
        return long(tm[:-2])
    raise ValueError(tm)



class FiveTuple(object):
    __slots__ = ['sourceAddress', 'destinationAddress', 'protocol', 'sourcePort', 'destinationPort']
    def __init__(self, el):
        self.sourceAddress = el.get('sourceAddress')
        self.destinationAddress = el.get('destinationAddress')
        self.sourcePort = int(el.get('sourcePort'))
        self.destinationPort = int(el.get('destinationPort'))
        self.protocol = int(el.get('protocol'))
        
class Histogram(object):
    __slots__ = 'bins', 'nbins', 'number_of_flows'
    def __init__(self, el=None):
        self.bins = []
        if el is not None:
            #self.nbins = int(el.get('nBins'))
            for bin in el.findall('bin'):
                self.bins.append( (float(bin.get("start")), float(bin.get("width")), int(bin.get("count"))) )

class Flow(object):
    __slots__ = ['flowId', 'delayMean', 'packetLossRatio', 'rxBitrate', 'txBitrate',
                 'fiveTuple', 'packetSizeMean', 'probe_stats_unsorted', 'delaySum', 'timeLastRxPacket',
                 'hopCount', 'flowInterruptionsHistogram', 'rx_duration','rxBytes','timeFirstTxPacket']
    def __init__(self, flow_el):
        self.flowId = int(flow_el.get('flowId'))
        rxPackets = long(flow_el.get('rxPackets'))
        txPackets = long(flow_el.get('txPackets'))
        self.rxBytes = long(flow_el.get('rxBytes'))
        tx_duration = (float(flow_el.get('timeLastTxPacket')[1:-4]) - float(flow_el.get('timeFirstTxPacket')[1:-4]))*1e-9
        rx_duration = (float(flow_el.get('timeLastRxPacket')[1:-4]) - float(flow_el.get('timeFirstRxPacket')[1:-4]))*1e-9
        self.timeLastRxPacket = (long)(flow_el.get('timeLastRxPacket')[1:-4])
        self.timeFirstTxPacket = (long)(flow_el.get('timeFirstTxPacket')[1:-4])
        self.rx_duration = rx_duration
#        self.rx_duration = (self.timeLastRxPacket - self.timeFirstTxPacket) * 1e-9
        self.probe_stats_unsorted = []
        self.delaySum = float(flow_el.get('delaySum')[1:-4])
        if rxPackets:
            self.hopCount = float(flow_el.get('timesForwarded')) / rxPackets + 1
        else:
            self.hopCount = -1000
        if rxPackets:
            self.delayMean = float(flow_el.get('delaySum')[1:-4]) / rxPackets * 1e-9
            self.packetSizeMean = float(flow_el.get('rxBytes')) / rxPackets
        else:
            self.delayMean = None
            self.packetSizeMean = None
        if rx_duration > 0:
            self.rxBitrate = long(flow_el.get('rxBytes'))*8 / (rx_duration)
        else:
            self.rxBitrate = None
        if tx_duration > 0:
            self.txBitrate = long(flow_el.get('txBytes'))*8 / tx_duration
        else:
            self.txBitrate = None
        lost = float(flow_el.get('lostPackets'))
        #print "rxBytes: %s; txPackets: %s; rxPackets: %s; lostPackets: %s" % (flow_el.get('rxBytes'), txPackets, rxPackets, lost)
        if rxPackets == 0:
            self.packetLossRatio = None
        else:
            self.packetLossRatio = (lost / (rxPackets + lost))

        interrupt_hist_elem = flow_el.find("flowInterruptionsHistogram")
        if interrupt_hist_elem is None:
            self.flowInterruptionsHistogram = None
        else:
            self.flowInterruptionsHistogram = Histogram(interrupt_hist_elem)


class ProbeFlowStats(object):
    __slots__ = ['probeId', 'packets', 'bytes', 'delayFromFirstProbe']

class Simulation(object):
    def __init__(self, simulation_el):
        self.flows = []
        FlowClassifier_el, = simulation_el.findall("Ipv4FlowClassifier")
        flow_map = {}
        for flow_el in simulation_el.findall("FlowStats/Flow"):
            flow = Flow(flow_el)
            flow_map[flow.flowId] = flow
            self.flows.append(flow)
        for flow_cls in FlowClassifier_el.findall("Flow"):
            flowId = int(flow_cls.get('flowId'))
            flow_map[flowId].fiveTuple = FiveTuple(flow_cls)

        for probe_elem in simulation_el.findall("FlowProbes/FlowProbe"):
            probeId = int(probe_elem.get('index'))
            for stats in probe_elem.findall("FlowStats"):
                flowId = int(stats.get('flowId'))
                s = ProbeFlowStats()
                s.packets = int(stats.get('packets'))
                s.bytes = long(stats.get('bytes'))
                s.probeId = probeId
                if s.packets > 0:
                    s.delayFromFirstProbe =  parse_time_ns(stats.get('delayFromFirstProbeSum')) / float(s.packets)
                else:
                    s.delayFromFirstProbe = 0
                flow_map[flowId].probe_stats_unsorted.append(s)



def main(argv):
    # file_obj = open(argv[1])
    # print "Reading XML file ",epresented in character sequences is signaled by a special character: the null character, whose literal value can be written as '\0' (backslash, zero).
#    loads = ['0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8']
    loads = ['0.6']
    f = open('rawdata', 'w') 
    tSim = 0.25
    means_rep = [0 for i in range(len(loads))] # 1-D list to store mean
    means_norep = [0 for i in range(len(loads))]
    mean_m_norep = [0 for i in range(len(loads))]
    mean_m_rep = [0 for i in range(len(loads))]
    mean_l_norep = [0 for i in range(len(loads))]
    mean_l_rep = [0 for i in range(len(loads))]

    tail_rep = [0 for i in range(len(loads))]
    tail_norep = [0 for i in range(len(loads))]
    tail_m_rep = [0 for i in range(len(loads))]
    tail_m_norep = [0 for i in range(len(loads))]
    tail_l_rep = [0 for i in range(len(loads))]
    tail_l_norep = [0 for i in range(len(loads))]

    tail_repdot = [0 for i in range(len(loads))]
    tail_norepdot = [0 for i in range(len(loads))]
    tail_m_repdot = [0 for i in range(len(loads))]
    tail_m_norepdot = [0 for i in range(len(loads))]
    tail_l_repdot = [0 for i in range(len(loads))]
    tail_l_norepdot = [0 for i in range(len(loads))]
    
    mean_total_norep = [0 for i in range(len(loads))]
    mean_total_rep = [0 for i in range(len(loads))]
    tail_total_norep = [0 for i in range(len(loads))]
    tail_total_rep = [0 for i in range(len(loads))]
    tail_total_norepdot = [0 for i in range(len(loads))]
    tail_total_repdot = [0 for i in range(len(loads))]

    improve_mean = [0 for i in range(len(loads))]
    improve_tail = [0 for i in range(len(loads))]
    improve_tail_dot = [0 for i in range(len(loads))]
    improve_mean_m = [0 for i in range(len(loads))]
    improve_tail_m = [0 for i in range(len(loads))]
    improve_tail_m_dot = [0 for i in range(len(loads))]
    improve_mean_l = [0 for i in range(len(loads))]
    improve_tail_l = [0 for i in range(len(loads))]
    improve_tail_l_dot = [0 for i in range(len(loads))]
    improve_mean_total = [0 for i in range(len(loads))]
    improve_tail_total = [0 for i in range(len(loads))]
    improve_tail_total_dot = [0 for i in range(len(loads))]

    tbytes = [0 for i in range(len(loads))]
    tbytes_tiny = [0 for i in range(len(loads))]
    delay = [0 for i in range(len(loads))]
    index = 0;
    for load in loads:
	times = array('d')
	mice_times = array('d')
	medium_times = array('d')
	elephant_times = array('d')
	totalbytes = 0
	for run in range(1,2):
	    fname = 'Run'+str(run)+'-'+ str(tSim) +'-'+load+'-4'+'.xml'
	    file_obj = open(fname)
 	    sys.stdout.flush()        
	    level = 0
	    sim_list = []
	    for event, elem in ElementTree.iterparse(file_obj, events=("start", "end")):
	        if event == "start":
	       	   level += 1
	        if event == "end":
	    	   level -= 1
		   if level == 0 and elem.tag == 'FlowMonitor':
		      sim = Simulation(elem)
		      sim_list.append(sim)
		      elem.clear() # won't need this any more
		      sys.stdout.write(".")
		      sys.stdout.flush()
	    	for sim in sim_list:
	 	    for flow in sim.flows:
		        t = flow.fiveTuple
		        if t.sourcePort >= 49153 :
		           proto = {6: 'TCP', 17: 'UDP'} [t.protocol]
			   # flow.timeLastRxPacket <= tSim*1e9+1e8 is used to exclude abnormal flows
		           if flow.rxBitrate != None and flow.timeFirstTxPacket <= tSim*1e9 and (flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003) < 1000:
			      
			      #if flow.rxBitrate != None:
			      times.append((flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003))
	#	                  times.append((flow.rx_duration)/(flow.rxBytes*8/1e10))
			      totalbytes = totalbytes + (flow.rxBytes);
			
		   	      delay.append(flow.delaySum)
			      if (flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003) > 100:
				      print flow.flowId, (flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003);	
			      if flow.rxBytes <= 100000:
				  
	#                            print flow.flowId, flow.rxBytes;
			          mice_times.append((flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003)); 
 		
			      elif flow.rxBytes <= 1000000:
			           medium_times.append((flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003)); 
				   
			      else: 
	#		           elephant_times.append((flow.rx_duration)/(flow.rxBytes*8/1e10+0.0000012)); 
		                   elephant_times.append((flow.rx_duration)/(flow.rxBytes*8/1e10+0.000003));
				
     
	means_norep[index] = (numpy.mean(mice_times))
        tail_norep[index] = (stats.scoreatpercentile(mice_times,95))
        tail_norepdot[index] = (stats.scoreatpercentile(mice_times,99))

        mean_m_norep[index] = (numpy.mean(medium_times))
        tail_m_norep[index] = (stats.scoreatpercentile(medium_times,95))
        tail_m_norepdot[index] = (stats.scoreatpercentile(medium_times,99))

        mean_l_norep[index] = (numpy.mean(elephant_times))
        tail_l_norep[index] = (stats.scoreatpercentile(elephant_times,95))
        tail_l_norepdot[index] = (stats.scoreatpercentile(elephant_times,99))

        mean_total_norep[index] = (numpy.mean(times))
        tail_total_norep[index] = (stats.scoreatpercentile(times,95))
        tail_total_norepdot[index] = (stats.scoreatpercentile(times,99))

        tbytes[index] = totalbytes

# print small, medium, large and total
        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (means_norep[index], tail_norep[index],tail_norepdot[index],\
	    		mean_m_norep[index], tail_m_norep[index],tail_m_norepdot[index], mean_l_norep[index], tail_l_norep[index],tail_l_norepdot[index], mean_total_norep[index], tail_total_norep[index],tail_total_norepdot[index] ))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (means_rep[index], tail_rep[index],tail_repdot[index],\
#            mean_m_rep[index], tail_m_rep[index], tail_m_repdot[index], mean_l_rep[index], tail_l_rep[index], tail_l_repdot[index], mean_total_rep[index], tail_total_rep[index], tail_total_repdot[index]))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (improve_mean[index], improve_tail[index],improve_tail_dot[index],\
#            improve_mean_m[index], improve_tail_m[index], improve_tail_m_dot[index], improve_mean_l[index], improve_tail_l[index], improve_tail_l_dot[index], improve_mean_total[index], improve_tail_total[index], improve_tail_total_dot[index]))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (improve_mean[index], improve_tail[index],\
#            improve_mean_m[index], improve_tail_m[index], improve_mean_l[index], improve_tail_l[index], improve_mean_total[index], improve_tail_total[index]))

# only print small, large and total
#	f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (means_norep[index], tail_norep[index],tail_norepdot[index],\
#	    mean_l_norep[index], tail_l_norep[index],tail_l_norepdot[index], mean_total_norep[index], tail_total_norep[index],tail_total_norepdot[index] ))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (means_rep[index], tail_rep[index],tail_repdot[index],\
#            mean_l_rep[index], tail_l_rep[index], tail_l_repdot[index], mean_total_rep[index], tail_total_rep[index], tail_total_repdot[index]))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (improve_mean[index], improve_tail[index],improve_tail_dot[index],\
#            improve_mean_l[index], improve_tail_l[index], improve_tail_l_dot[index], improve_mean_total[index], improve_tail_total[index], improve_tail_total_dot[index]))
	# print "%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t " % (means_norep[index], tail_norep[index],\
	    # mean_l_norep[index], tail_l_norep[index], mean_total_norep[index], tail_total_norep[index])
	# print "%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t " % (means_rep[index], tail_rep[index],\
	    # mean_l_rep[index], tail_l_rep[index], mean_total_rep[index], tail_total_rep[index])

#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (means_norep[index], tail_norep[index],\
#            mean_m_norep[index], tail_m_norep[index], mean_l_norep[index], tail_l_norep[index], mean_total_norep[index], tail_total_norep[index] ))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (means_rep[index], tail_rep[index],\
#           mean_m_rep[index], tail_m_rep[index], mean_l_rep[index], tail_l_rep[index], mean_total_rep[index], tail_total_rep[index]))
#        f.write('%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\n' % (improve_mean[index], improve_tail[index],\
#            improve_mean_m[index], improve_tail_m[index], improve_mean_l[index], improve_tail_l[index], improve_mean_total[index], improve_tail_total[index]))


	index += 1
    f.close()

if __name__ == '__main__':
    main(sys.argv)
