#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.24.1-scratch-simulator-debug', 'build/scratch/subdir/ns3.24.1-subdir-debug', 'build/scratch/ns3.24.1-LeafSpine-debug', 'build/scratch/ns3.24.1-FatTree-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

